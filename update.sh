#!/usr/bin/env bash

set -euo pipefail

if [ -z "$VIRTUAL_ENV" ]; then
    echo "Please activate your virtualenv first."
    exit 1
fi
DATE="$(date --iso-8601=seconds --utc)"
git -C submodules/integration/config pull
git commit -a -m "${DATE} (submodules/integration/config: $(git -C submodules/integration/config rev-parse --short HEAD))"
make clean && make

REPOS="$(awk -F':' '/repos/ {print $2}' projects.txt | xargs)"
git add .
rm README.md
make README.md
git commit -a -m "${DATE} (repos: ${REPOS})"
rm README.md
make README.md
git commit -a -m "${DATE} Update README.md"
