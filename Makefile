.PHONY: clean
DATE=$(shell date -I --utc)

README.md: projects.txt
	jupyter nbconvert --to notebook --inplace --execute "generate-repo_count_over_time.ipynb"
	echo '# Gerrit repos' > "$@"
	printf '![Gerrit repos as of ' >> "$@"
	printf '%s' "$(DATE)" >> "$@"
	printf '](repo_count_over_time.png)\n' >> "$@"
	echo '```' >> "$@"
	cat projects.txt >> "$@"
	echo '```' >> "$@"

projects.txt: projects.db
	printf 'There are this many repos left to move to gitlab: ' > "$@"
	echo 'select count(*) from project;' | sqlite3 projects.db >> "$@"
	printf 'Repos updated in the past year (on gerrit): ' >> "$@"
	echo 'SELECT count(*) FROM project WHERE updated >= strftime("%s", "now", "-1 year");' | sqlite3 projects.db >> "$@"
	printf 'Repos with tests (on gerrit): ' >> "$@"
	echo 'SELECT count(*) FROM project WHERE tests is not null;' | sqlite3 projects.db >> "$@"
	printf 'operations/* repos in gerrit: ' >> "$@"
	echo 'select count(*) from project WHERE name like "operations/%";' | sqlite3 projects.db >> "$@"
	printf 'operations/* repos with tests in gerrit: ' >> "$@"
	echo 'SELECT count(*) FROM project WHERE name like "operations/%" and tests is not null;' | sqlite3 projects.db >> "$@"
	printf 'operations/* repos updated in the past year in gerrit: ' >> "$@"
	echo 'SELECT count(*) FROM project WHERE name like "operations/%" and updated >= strftime("%s", "now", "-1 year");' | sqlite3 projects.db >> "$@"
	echo '* * * * * * * * * * * * * * * * * * * * *' >> "$@"
	echo 'select name from project;' | sqlite3 projects.db >> "$@"

projects.db: active-gerrit-repos-$(DATE).tsv active-diffusion-repos-$(DATE).tsv
	python3 03-active-gerrit-projects-with-tests.py

active-gerrit-repos-$(DATE).tsv:
	python3 01-get-active-gerrit-projects.py

active-diffusion-repos-$(DATE).tsv:
	python3 02-active-diffusion-repos.py

clean:
	[ -e projects.db ] && mv projects.db projects-$(DATE).old.db || :
	rm -rf README.md projects.txt *-$(DATE).tsv
