#!/usr/bin/env python

import json
import requests
import subprocess
import urllib

from email.utils import parsedate_to_datetime
from datetime import datetime, timezone

SESSION = requests.Session()
today = datetime.now(tz=timezone.utc).strftime('%Y-%m-%d')
r = SESSION.get('https://gerrit.wikimedia.org/r/projects/?type=CODE')
repos = json.loads(r.text[5:])
active_repos = [r for r in repos if repos[r]['state'] == 'ACTIVE']

old_date = parsedate_to_datetime('Mon, 01 Jan 1900 00:00:00 -0700')

repo_update_list = []
for repo in active_repos:
    last_updated = old_date
    repo_name_url = urllib.parse.quote_plus(repo)
    print(repo)
    refs = subprocess.check_output(
        'git ls-remote --heads https://gerrit.wikimedia.org/r/{}'.format(repo),
        shell=True
    ).strip().decode('utf8')
    for ref in refs.splitlines():
        sha, ref = ref.split('\t')
        if ref.startswith('refs/heads/wmf/1.') or ref.startswith('refs/heads/REL1_') or ref == 'wmf/branch_cut_pretest':
            continue
        print(ref)
        x = SESSION.get('https://gerrit.wikimedia.org/g/{}/+/{}?format=JSON'.format(
            repo_name_url,
            sha
        ))
        try:
            updated = parsedate_to_datetime(json.loads(x.text[5:])['committer']['time'])
        except:
            continue
        if updated > last_updated:
            last_updated = updated
    final = '{}\t{}'.format(repo, last_updated.strftime('%Y-%m-%d'))
    repo_update_list.append(final)
    print(final)

with open(f'active-gerrit-repos-{today}.tsv', 'w') as f:
    f.write('\n'.join(repo_update_list))

print('{} active gerrit repos'.format(len(repo_update_list)))
