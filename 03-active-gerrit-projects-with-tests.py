#!/usr/bin/env python3

import copy
import json
import sqlite3
import yaml

from datetime import datetime, timezone

DATE = datetime.now(tz=timezone.utc).strftime('%Y-%m-%d')
DB_PATH = 'projects.db'
TEMPLATES = {}
GERRIT_URL_TEMP = 'https://gerrit.wikimedia.org/r/{}'


class Repo:
    def __init__(self, name, source, url, updated):
        self.name = name
        self.source = source
        self.url = url
        self.updated = updated
        self.tests = None
        self.zuul_raw = None

    def get_zuul_raw(self):
        if self.zuul_raw is None:
            return self.zuul_raw
        return json.dumps(self.zuul_raw)

    def get_tests(self):
        if self.tests is None:
            return self.tests
        return json.dumps(self.tests)

    def get_updated(self):
        return datetime.strptime(self.updated, '%Y-%m-%d').timestamp()


def get_proj_tests(project):
    """Flatten all the tests in zuul."""
    if project.get('name'):
        _ = project.pop('name')

    tests = []
    for key in project:
        tests += project[key]
    return tests


def setup_db():
    conn = sqlite3.connect(DB_PATH)
    crs = conn.cursor()
    crs.execute('''
        CREATE TABLE IF NOT EXISTS project (
            id INTEGER PRIMARY KEY,
            name TEXT UNIQUE NOT NULL,
            url TEXT UNIQUE NOT NULL,
            source TEXT NOT NULL,
            tests TEXT,
            zuul_raw TEXT,
            updated INTEGER NOT NULL,
            CHECK (source IN ('gerrit', 'phabricator'))
        )
    ''')
    conn.commit()
    return conn


def get_zuul_layout():
    with open('submodules/integration/config/zuul/layout.yaml') as f:
        zuul_layout = yaml.safe_load(f.read())
    for template in zuul_layout['project-templates']:
        n = template['name']
        _ = template.pop('name')
        TEMPLATES[n] = template

    # all_pipelines = set([item for sublist in [
    #         [*templates[t].keys()]
    #         for t in templates.keys()
    #     ] for item in sublist])

    return zuul_layout

def get_phab_repos():
    repo_objs = []
    source = 'phabricator'
    with open(f'active-diffusion-repos-{DATE}.tsv') as f:
        repos = [
            tuple(x.strip().split('\t'))
            for x in f.readlines()
        ]
    for repo in repos:
        repo_objs.append(Repo(
            name=repo[0],
            url=repo[1],
            updated=repo[2],
            source=source
        ))

    return repo_objs


def get_gerrit_repos(zuul_layout):
    repo_objs = []
    source = 'gerrit'
    with open(f'active-gerrit-repos-{DATE}.tsv') as f:
        repos = [
            tuple(x.strip().split('\t'))
            for x in f.readlines()
            if x.strip().split('\t')[1] != '1900-01-01'  # this means the repo's empty
         ]
    for repo in repos:
        repo_name = repo[0]
        repo_obj = Repo(
            name=repo_name,
            url=GERRIT_URL_TEMP.format(repo_name),
            updated=repo[1],
            source=source
        )

        for proj in zuul_layout['projects']:
            if repo_obj.name == proj['name']:
                project_copy = copy.deepcopy(proj)
                if project_copy.get('template'):
                    proj_templates = project_copy.pop('template')
                    for tmp in proj_templates:
                        erg = TEMPLATES.get(tmp['name'])
                        if erg:
                            project_copy.update(erg)
                repo_obj.zuul_raw = project_copy
                repo_obj.tests = get_proj_tests(project_copy)

        repo_objs.append(repo_obj)
    return repo_objs

def main():
    conn = setup_db()
    crs = conn.cursor()
    zuul_layout = get_zuul_layout()
    gerrit_repos = get_gerrit_repos(zuul_layout)
    phab_repos = get_phab_repos()

    repos = gerrit_repos + phab_repos

    for repo in repos:
        try:
            crs.execute('''
                INSERT INTO project(
                    name,
                    url,
                    source,
                    tests,
                    zuul_raw,
                    updated)
                VALUES(?,?,?,?,?,?)''', (
                    repo.name,
                    repo.url,
                    repo.source,
                    repo.get_tests(),
                    repo.get_zuul_raw(),
                    repo.get_updated()
                )
            )
            conn.commit()
        except:
            import pdb
            pdb.set_trace()
            raise


if __name__ == '__main__':
    main()
