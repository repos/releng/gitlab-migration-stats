#!/usr/bin/env python3

import json
import os
import requests

from datetime import datetime, timezone


DATE = datetime.now(tz=timezone.utc).strftime('%Y-%m-%d')
OUTPUT = 'active-diffusion-repos-{}.tsv'.format(DATE)


def flatten_for_post(h, result=None, kk=None):
    """
    Since phab expects x-url-encoded form post data (meaning each
    individual list element is named). AND because, evidently, requests
    can't do this for me, I found a solution via stackoverflow.

    See also:
    <https://secure.phabricator.com/T12447>
    <https://stackoverflow.com/questions/26266664/requests-form-urlencoded-data/36411923>
    """
    if result is None:
        result = {}

    if isinstance(h, str) or isinstance(h, bool):
        result[kk] = h
    elif isinstance(h, list) or isinstance(h, tuple):
        for i, v1 in enumerate(h):
            flatten_for_post(v1, result, '%s[%d]' % (kk, i))
    elif isinstance(h, dict):
        for (k, v) in h.items():
            key = k if kk is None else "%s[%s]" % (kk, k)
            if isinstance(v, dict):
                for i, v1 in v.items():
                    flatten_for_post(v1, result, '%s[%s]' % (key, i))
            else:
                flatten_for_post(v, result, key)
    return result


class Phab(object):
    def __init__(self):
        self.phab_url = 'https://phabricator.wikimedia.org/api/'
        self.conduit_token = self._get_token()

    def _get_token(self):
        """
        Use the $CONDUIT_TOKEN envvar, fallback to whatever is in ~/.arcrc
        """
        token = None
        token_path = os.path.expanduser('~/.arcrc')
        if os.path.exists(token_path):
            with open(token_path) as f:
                arcrc = json.load(f)
                token = arcrc['hosts'][self.phab_url]['token']

        return os.environ.get('CONDUIT_TOKEN', token)

    def _query_phab(self, method, data):
        """
        Helper method to query phab via requests and return json
        """
        data = flatten_for_post(data)
        data['api.token'] = self.conduit_token
        r = requests.post(
            os.path.join(self.phab_url, method),
            data=data)
        r.raise_for_status()
        return r.json()

    def find_uris(self, private_count=0, ret=None, after=None):
        """
        Get a set of authors from a list of commit sha1s
        """
        data = {
                "queryKey": "all",
                "attachments": {
                    'uris': '1',
                },
            }

        if ret is None:
            ret = set()

        if after:
            data['after'] = after

        repos = self._query_phab(
            'diffusion.repository.search',
            data
        )

        results = repos['result']

        if not results:
            with open(OUTPUT, 'w') as f:
                f.write('\n'.join(ret))
            return ret

        for repo in results['data']:
            on_diffusion = True
            if repo['fields']['status'] == 'inactive':
                continue

            repo_name = '{} (diffusion)'.format(repo['fields']['name'])
            last_updated = datetime.fromtimestamp(repo['fields']['dateModified']).strftime('%Y-%m-%d')

            for uri in repo['attachments']['uris']['uris']:
                # Looking only for uris that are pointing to gerrit
                # that are also of io type "observe"
                io  = uri['fields']['io']['raw']
                display_uri = uri['fields']['uri']['display']
                if display_uri.startswith('https://'):
                    repo_link = display_uri
                if io == 'observe':
                    on_diffusion = False

            if on_diffusion is True:
                print('{}\t{}\t{}'.format(repo_name, repo_link, last_updated))

                if repo['fields']['policy']['view'] != 'public':
                    repo_name = 'private repo {}'.format(private_count)
                    repo_link = 'private repo {}'.format(private_count)
                    private_count += 1

                ret.add('{}\t{}\t{}'.format(repo_name, repo_link, last_updated))

        after = results['cursor']['after']
        if after:
            print('.')
            self.find_uris(private_count=private_count, ret=ret, after=after)

        else:
            with open(OUTPUT, 'w') as f:
                f.write('\n'.join(ret))
            return ret

def main():
    p = Phab()
    p.find_uris()

if __name__ == '__main__':
    main()
